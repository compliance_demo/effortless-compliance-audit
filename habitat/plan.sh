pkg_name=compliance-audit-demo
pkg_version=0.1.0
pkg_origin=gsloan-chef
pkg_maintainer="The Habitat Maintainers <humans@habitat.sh>"
pkg_license=("Apache-2.0")
pkg_description="Effortless Linux Audit Example"
pkg_scaffolding="chef/scaffolding-chef-inspec"
scaffold_chef_license="accept-no-persist"
scaffold_automate_server_url=$AUTOMATE_SERVER_URL # Example: https://foo.bar.com
scaffold_automate_user=$AUTOMATE_USER             # Example: "admin"
scaffold_automate_token=$AUTOMATE_TOKEN           # Example: "DI0WVxInnyGnWKRlZBGizTXySgk="
